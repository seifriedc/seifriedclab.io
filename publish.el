;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Rasmus

(package-initialize)
(require 'org)
(require 'ox-publish)
(setq user-full-name "Chris Seifried")
(setq user-mail-address "seifried.chris@gmail.com")
(setq org-publish-use-timestamps-flag nil)
(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-doctype "html5")
(defvar site-attachments (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                                       "ico" "cur" "css" "js" "woff" "html" "pdf")))

(defconst website-html-head
  "<link rel='stylesheet' href='/res/site.css' type='text/css'/>")

(defconst website-html-blog-head
  "<link rel='stylesheet' href='../res/site.css' type='text/css'/>")

(defconst website-html-preamble
  "<h1>Chris Seifried</h1>
<div class='nav'>
<ul>
<li><a href='/index.html'>About Me</a></li>
<li><a href='/blog/sitemap.html'>Blog</a></li>
<li><a href='http://github.com/seifriedc'>GitHub</a></li>
<li><a href='/contact.html'>Contact</a></li>
</ul>
</div>")

(setq org-publish-project-alist
      (list
       (list "blog"
             :base-directory "blog/"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "public/blog/"
             :exclude (regexp-opt '("README" "draft"))
             :auto-sitemap t
             :sitemap-filename "sitemap.org"
             :sitemap-title "Blog Entries"
             :sitemap-file-entry-format "%d *%t*"
             :html-head website-html-blog-head
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"../res/favicon.ico\"/>"
             :html-preamble website-html-preamble
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically)
       (list "pages"
             :base-directory "pages/"
             :base-extension "org"
             :publishing-directory "public/"
             :publishing-function '(org-html-publish-to-html)
             :html-head website-html-head
             :html-preamble website-html-preamble
             :recursive t)
       (list "res"
             :base-directory "res/"
             :base-extension site-attachments
             :publishing-function 'org-publish-attachment
             :publishing-directory "public/res/"
             :recursive t)
       (list "site" :components '("blog" "pages" "res"))))
